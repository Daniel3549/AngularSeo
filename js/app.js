var app = angular.module('app', ['appControllers','ngRoute','appDirectives']);

app.config(['$routeProvider','$locationProvider',function($routeProvider,$locationProvider) {
	$routeProvider.
	when('/', {
		templateUrl: 'views/home.html',
        controller: 'mainCtrl'
	}).
	when('/ajaxseo', {
		templateUrl: 'views/ajaxseo.html',
        controller: 'ajaxSeoCtrl'
	}).
	when('/instantaneas', {
		templateUrl: 'views/instantaneas.html',
        controller: 'instantaneasCtrl'
	}).
	otherwise({
        redirectTo: '/'
    });
    $locationProvider.html5Mode(false);
	$locationProvider.hashPrefix('!');
}])